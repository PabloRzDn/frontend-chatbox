import React from 'react'
import { Navigate, Outlet } from 'react-router-dom'



const LoginGuard = () => {
  return (
    localStorage.getItem("access") ? <Outlet/>:<Navigate replace to={"/login"}/>
  )
}

export default LoginGuard