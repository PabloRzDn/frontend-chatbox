import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import ChatComponent from "../components/Chat/ChatComponent";
import FrasesComponent from "../components/Frases/FrasesComponent";
import Layout from "../components/Layout/Layout";
import IndexComponent from "../components/Index/IndexComponent";
import LoginComponent from "../components/Login/LoginComponent";
import LoginGuard from "../guards/Login.guard";
import LoginLayout from "../components/Layout/LoginLayout";

export const AppRoutes = () => {
  return (
    <Routes>
      <Route path={"/login"} element={ <LoginComponent />} />

      <Route element={<LoginGuard />}>
        <Route path={"/index"} element={<Layout children={<IndexComponent />} pageTitle={"Inicio"} />} />
        <Route path="/" element={<Navigate to={"/login"} />} />
        <Route path="*" element={<Navigate to={"/login"} />} />
        <Route path={"/chat"} element={<Layout children={<ChatComponent />} pageTitle={"Chat"} />} />
        <Route path={"/frases"} element={<Layout children={<FrasesComponent />} pageTitle={"Frases"} />} />
      </Route>
    </Routes>
  );
};
