import { createChatBotMessage } from 'react-chatbot-kit';
import DogPicture from './components/DogPicture.jsx'
const botName = 'Agente virtual EOL';

const config = {
  initialMessages: [createChatBotMessage(`Hola! Soy el ${botName}`)],
  widgets: [
    {
      widgetName: 'dogPicture',
      widgetFunc: (props) => <DogPicture {...props} />,
    },
  ],
  customStyles: {
    botMessageBox: {
      backgroundColor: '#376B7E',
    },
    chatButton: {
      backgroundColor: '#5ccc9d',
    },}
};


/*
const config = {
  initialMessages: [createChatBotMessage(`Hola! Soy el ${botName}`)],
  botName: botName,
  customStyles: {
    botMessageBox: {
      backgroundColor: '#376B7E',
    },
    chatButton: {
      backgroundColor: '#5ccc9d',
    },
  },
};
*/

export default config;