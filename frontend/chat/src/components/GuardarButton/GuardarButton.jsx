import React from 'react'
import { Button } from '@mui/material'
import AddIcon from '@mui/icons-material/Add';

const GuardarButton = ({disabled, onClickGuardar}) => {
  return (
    <Button
    disabled={disabled}
    size="large"
    onClick={onClickGuardar}
    color="success"
    title="Guardar"
    variant="outlined"
>
    Guardar
</Button>
  )
}

export default GuardarButton