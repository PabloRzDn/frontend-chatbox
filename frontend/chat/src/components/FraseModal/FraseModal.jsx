import React, { useState } from "react";
import CrearFraseButton from "../CrearFraseButton/CrearFraseButton";
import { Card, Button, Dialog, DialogTitle, DialogActions, DialogContent, DialogContentText, Grid } from "@mui/material";
import { useModal } from "../../Hooks/UseModal";
import FraseInput from "../FraseInput/FraseInput";
import { dummyEtiqueta } from "../../sandbox/dummyData";
import { dummyTipoMensaje } from "../../sandbox/dummyData";
import EtiquetaSelector from "../EtiquetaSelector/EtiquetaSelector";
import TipoMensajeSelector from "../TipoMensajeSelector/TipoMensajeSelector";
import GuardarButton from "../GuardarButton/GuardarButton";
import { successAlert, dangerAlert } from "../../Alert/Alert";
import { postFrase } from "../../redux/actions/frase.actions";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getEtiquetas } from "../../redux/actions/etiquetas.actions";
import { getTipos } from "../../redux/actions/tipo.actions";

const FraseModal = ({refreshTable}) => {
  const dispatch = useDispatch()
  const etiquetas = useSelector(store => store.etiquetas.results)
  const tipos = useSelector(store => store.tipos.results)

  const [isOpenModal, openModal, closeModal] = useModal(false);
  const [contenido, setContenido] = useState({
    frase: "",
    tipoMensaje: "",
    etiqueta: "",
  });

  const { frase, tipoMensaje, etiqueta } = contenido;
  
  
  useEffect(()=>{
    dispatch(getEtiquetas())
    dispatch(getTipos())
  },[])
  

  const onClickOpenModal = () => {
    openModal();
  };




  const handleClose = () => {
    setContenido({
      frase: "",
      tipoMensaje: "",
      etiqueta: "",
    });
    closeModal();
  };

  const handleChange = (key) => (event) => {
    setContenido({ ...contenido, [key]: event.target.value });
  };





  const onClickGuardar = () => {
    closeModal();
    console.log(contenido)
    postFrase({
      cuerpo: contenido.frase, 
      tipo_mensaje: contenido.tipoMensaje, 
      etiqueta: contenido.etiqueta}).then( res => {
        if(res.status === 201){
          successAlert(res.data.Frase);
          refreshTable()
        } else {
          dangerAlert("Hubo un problema")
        }
      })
    
    setContenido({
      frase: "",
      tipoMensaje: "",
      etiqueta: "",
    });
  };

  return (
    <div>
      <CrearFraseButton onClickCrearFrase={onClickOpenModal} disabled={false} />

      <Dialog open={isOpenModal} onClose={handleClose} fullWidth={true}>
        <DialogTitle>Crear Frase</DialogTitle>
        <DialogContent>
          <Grid container columnSpacing={1}>
            <Grid item mt={3} xs={12} xl={12}>
              <FraseInput
                required={false}
                maxLength={100}
                id={"frase"}
                name={"frase"}
                label={"Frase"}
                handleChange={handleChange("frase")}
                value={frase}
                disabled={false}
              />
            </Grid>
            <Grid item mt={3} mb={3} xl={7} xs={12}>
              <TipoMensajeSelector handleChange={handleChange("tipoMensaje")} currentValue={tipoMensaje} values={tipos} />
            </Grid>
            <Grid item mb={3} xl={7} xs={12}>
              <EtiquetaSelector handleChange={handleChange("etiqueta")} currentValue={etiqueta} values={etiquetas} />
            </Grid>
          </Grid>
        </DialogContent>
        <DialogActions>
          <GuardarButton disabled={false} onClickGuardar={onClickGuardar} />
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default FraseModal;
