import React from "react";
import { Button } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, { SelectChangeEvent } from "@mui/material/Select";

const EtiquetaSelector = ({handleChange, values, currentValue }) => {
  return (
    <div>
      <FormControl fullWidth={true}>
        <InputLabel id="demo-simple-select-autowidth-label">Etiqueta</InputLabel>
        <Select
          labelId="demo-simple-select-autowidth-label"
          id="demo-simple-select-autowidth"
          value={currentValue}
          onChange={handleChange}
          
          label="Etiqueta"
          
        >

          {
            values.map( (item,index) =>(
                <MenuItem key={index} value={item.id}>{item.etiqueta}</MenuItem>


            ))
          }
        </Select>
      </FormControl>
    </div>
  );
};

export default EtiquetaSelector;
