import {
    Container,
    Typography
} from '@mui/material'
import React from 'react'
import MiniDrawer from '../AppBar/AppBar'
import { loginStyles } from '../Login/LoginComponent.styles'

const LoginLayout = ({children}) => {
  return (
    <main>
         
        <Container
            style={loginStyles.loginContainer }
            maxWidth={true}
            disableGutters={true}
            >
            {children}
        </Container>
        
            
          
         
    </main>
  )
}

export default LoginLayout