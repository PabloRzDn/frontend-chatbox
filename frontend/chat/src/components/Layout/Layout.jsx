import {
    Container,
    Typography
} from '@mui/material'
import React from 'react'
import MiniDrawer from '../AppBar/AppBar'

const Layout = ({children, pageTitle}) => {
  return (
    <main>
         <MiniDrawer
          currentUser={"User"}
        >
        <Container
            fixed
            sx={{ marginTop: 3 }}
            maxWidth={false}
            disableGutters={false}>
            <Typography variant="h5" noWrap component="div">
                {pageTitle}
            </Typography>
        </Container>
        <Container fixed
            sx={{marginBottom:15}}
            maxWidth={false}
            disableGutters={true}>
            {children}
          </Container>
          </MiniDrawer>
    </main>
  )
}

export default Layout