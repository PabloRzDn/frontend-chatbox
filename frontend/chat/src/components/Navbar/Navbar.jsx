import React from "react";

import { Sidebar, ProSidebar, Menu, MenuItem, SubMenu, SidebarHeader, SidebarFooter, SidebarContent } from "react-pro-sidebar";
import 'react-pro-sidebar/dist/scss/styles.scss';
import ChatIcon from '@mui/icons-material/Chat';
import HomeIcon from "@mui/icons-material/Home";
import { Navigate, Link } from "react-router-dom";
import TextSnippetIcon from '@mui/icons-material/TextSnippet';
import IconButton from "@mui/material/IconButton";

const Navbar = ({ isOpen, handleOpen }) => {
  return (
    <ProSidebar collapsedWidth={60} collapsed={!isOpen}>
        <SidebarContent>
      <Menu>
        <MenuItem onClick={isOpen ? handleOpen : undefined} icon={<HomeIcon />}>
          Inicio
          <Link to={"/index"} />
        </MenuItem>
        <MenuItem onClick={isOpen ? handleOpen : undefined} icon={<ChatIcon  />}>
          Chat
          <Link to={"/chat"} />
        </MenuItem>
        <MenuItem onClick={isOpen ? handleOpen : undefined} icon={<TextSnippetIcon />}>
          Frases
          <Link to={"/frases"} />
        </MenuItem>
      </Menu>
      </SidebarContent>
    </ProSidebar>
  );
};

export default Navbar;
