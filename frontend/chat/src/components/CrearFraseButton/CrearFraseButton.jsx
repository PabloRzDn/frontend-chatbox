import React from 'react'
import { Button } from '@mui/material'
import AddIcon from '@mui/icons-material/Add';

const CrearFraseButton = ({disabled, onClickCrearFrase}) => {

  return (
    <Button
            disabled={disabled}
            size="medium"
            onClick={onClickCrearFrase}
            color="success"
            title="Crear Frase"
            variant="outlined"
            endIcon={<AddIcon />}
        >
            Crear Frase
        </Button>
  )
}

export default CrearFraseButton