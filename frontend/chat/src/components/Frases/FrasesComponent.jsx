import React from "react";
import FrasesTable from "./FrasesTable";
import { Grid } from "@mui/material";
import CrearFraseButton from "../CrearFraseButton/CrearFraseButton";
import FraseModal from "../FraseModal/FraseModal";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getFrases } from "../../redux/actions/frases.actions";

const FrasesComponent = () => {
  const dispatch = useDispatch();
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(10);

  const frases = useSelector((store) => store.frases.results);
  const count = useSelector((store) => store.frases.count);
  useEffect(() => {
    dispatch(getFrases({ page: 1, pageSize: rowsPerPage }));
  }, []);



  const handleChangePage = (e, newPage) => {
    setPage(newPage);
    const pageOnRequest = newPage + 1;
    dispatch(getFrases({ page: pageOnRequest, pageSize: rowsPerPage }));
  };

  const handleSelect = (e) => {
    const rows = e.target.value
    setRowsPerPage(rows)
    setPage(0)
    dispatch(getFrases({ page: 1, pageSize:rows }));
  };

  const refreshTable = () => {
    dispatch(getFrases({ page: 1, pageSize: rowsPerPage }))
  }

  return (
    <Grid container marginTop={3}>
      <Grid item xl={3} xs={12} marginBottom={3}>
        <FraseModal refreshTable={refreshTable}/>
      </Grid>

      <Grid item xs={12} xl={12}>
        <FrasesTable
          rows={frases}
          page={page}
          rowsPerPage={rowsPerPage}
          count={count}
          handleChangePage={handleChangePage}
          handleSelect={handleSelect}
        />
      </Grid>
    </Grid>
  );
};

export default FrasesComponent;
