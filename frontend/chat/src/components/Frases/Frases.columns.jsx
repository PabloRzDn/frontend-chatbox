export const columns =[
    
    { id: "ticket_id", label: "Ticket", minWidth: 65 },
    { id: "cuerpo", label: "Frases", minWidth: 120 },
    { id: "tipo", label: "Tipo Mensaje", minWidth: 120 },
    { id: "etiqueta", label: "Etiqueta", minWidth: 120 }
]