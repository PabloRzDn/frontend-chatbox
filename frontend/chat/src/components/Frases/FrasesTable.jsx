import React from 'react'
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import { styled } from '@mui/material/styles';
import { columns } from './Frases.columns';

const FrasesTable = ({ rows, page, rowsPerPage, count, handleChangePage, handleSelect }) => {
  return (
    <div>
        <Paper sx={{ width: '100%', overflow: 'hidden' }}>
                <TableContainer>
                    <Table size="small" aria-label="a dense table">
                        <TableHead style={{ height: 8 }}>
                            <TableRow  >

                                {columns.map((column, index) => (
                                    <TableCell

                                        key={index}
                                        align="center"
                                        style={{
                                            minWidth: column.minWidth,
                                            color: 'black',
                                            height: 8
                                        }}
                                    >
                                        {column.label}
                                    </TableCell>
                                ))}

                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rows?.map((row, index) => {
                                return (
                                    <TableRow 
                                    key={index} 
                                    >
                                        {
                                            columns.map((column, index) => {
                                                
                                                const value = row[column.id]
                                                return (
                                                    <TableCell
                                                        key={index}
                                                        align="center">
                                                        {value}
                                                    </TableCell>
                                                )
                                            })
                                        }
                                    </TableRow>
                                )
                            }
                            )}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    sx={{
                        ".MuiTablePagination-displayedRows": {
                            color: "black",
                        },
                        ".MuiTablePagination-selectLabel": {
                            color: "black",
                        },
                    }}
                    //data={rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)}
                    rowsPerPageOptions={[10, 50, 100]}
                    component="div"
                    count={ Math.floor( count )}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onPageChange={handleChangePage}
                    onRowsPerPageChange={handleSelect}
                    showFirstButton={true}
                    showLastButton={true}
                    labelDisplayedRows={
                        ({ page, from, to, count }) => {
                            let pagina = 1 + page
                            return (<span>Página  {pagina}  &nbsp;&nbsp;&nbsp;&nbsp;  {from}-{to}  de  {count} </span>)
                        }}
                    labelRowsPerPage="Filas por Página"
                />

            </Paper>
    </div>
  )
}

export default FrasesTable