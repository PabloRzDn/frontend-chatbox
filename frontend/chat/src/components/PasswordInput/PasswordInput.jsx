import React from 'react'
import { TextField } from '@mui/material'

const PasswordInput = ({ label, name, handleChange, value, id, disabled, maxLength, required }) => {
  return (
    <TextField
                type="password"
                required={required}
                inputProps={{ maxLength: maxLength }}
                id={id}
                name={name}
                label={label}
                value={value || ""}
                size='lg'
                variant="outlined"
                fullWidth
                onChange={handleChange}
                disabled={disabled} />
  )
}

export default PasswordInput