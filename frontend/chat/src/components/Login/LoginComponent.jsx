import React, { useState } from "react";
import { Grid, Paper, Container, Box, Typography, Card, CardContent, CardActions } from "@mui/material";
import IngresarButton from "../IngresarButton/IngresarButton";
import { loginStyles } from "./LoginComponent.styles";
import backgroundImage from "../../assets/wallpaper.jpg";
import PasswordInput from "../PasswordInput/PasswordInput";
import TextInput from "../TextInput/TextInput";
import { useNavigate } from "react-router-dom";
import { Navigate } from "react-router-dom";
import {useDispatch} from "react-redux"
import { login } from "../../redux/actions/user.actions";

const LoginComponent = () => {
  const dispatch = useDispatch()
  const [user, setUser] = useState({
    usuario: "",
    password: "",
  });

  const { usuario, password } = user;

  const navigate = useNavigate()

  const handleChange = (key) => (event) => {
    setUser({ ...user, [key]: event.target.value });
  };

  const onClickIngresar = (event) => {
    event.preventDefault()
    dispatch(login(user)).then( res =>{
      if(res.status===200){
        navigate('/index',{replace:true})
        localStorage.setItem("access",res.data.access)
      } else {
      }
    })
    
    //navigate('/index',{replace:true})
  }
  return (
    <Container
      style={{
        margin: 0,
        backgroundImage: `url(${backgroundImage})`,
        backgroundSize: "cover",
        height: "100vh",
        minWidth: "100vw",
        backgroundRepeat: "no-repeat",
      }}
    >
      <Grid
        container
        sx={{
          height: "100%",
          //marginBotton:"10%",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Card sx={{ width: "35%" }}>
        <form>
          <CardContent>
            <Grid item mx={3} my={3} lx={12}>
              <TextInput
                required={false}
                maxLength={100}
                id={"usuario"}
                name={"usuario"}
                label={"Usuario"}
                handleChange={handleChange("usuario")}
                value={usuario}
                disabled={false}
              />
            </Grid>
            <Grid item mx={3} my={3} lx={12}>
              <PasswordInput
                required={false}
                maxLength={100}
                id={"password"}
                name={"password"}
                label={"Contraseña"}
                handleChange={handleChange("password")}
                value={password}
                disabled={false}
              />
            </Grid>
          </CardContent>
          <CardActions>
            <Grid item mx={3} my={3} xl={12}>
              <IngresarButton disabled={false} onClickIngresar={(e) => onClickIngresar(e)} />
            </Grid>
          </CardActions>
          </form>
        </Card>
        
      </Grid>
    </Container>
  );
};

export default LoginComponent;
