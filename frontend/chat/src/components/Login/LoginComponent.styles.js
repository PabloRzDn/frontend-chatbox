import backgroundImage from "../../assets/wallpaper.jpg"

export const loginStyles = {
  loginContainer: {
      height: "100vh",
      backgroundImage: `url(${backgroundImage})`,
      backgroundSize: "cover",
      backgroundPosition: "center",
      width: "100vw",
     
    },
};
