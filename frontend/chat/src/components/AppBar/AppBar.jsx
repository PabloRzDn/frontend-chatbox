import React from "react"
import { styled, useTheme, Theme, CSSObject } from '@mui/material/styles';
import { Box } from "@mui/material"
import MuiDrawer from '@mui/material/Drawer';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';

import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import Navbar from "../Navbar/Navbar";
import './AppBar.css'
const drawerWidth = 240;

const openedMixin = (theme) => ({
    width: drawerWidth,
    justifyContent:"end",
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
    }),
    overflowX: 'hidden',
    color:'black !important'
});

const closedMixin = (theme) => ({
    transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: `calc(${theme.spacing(7)} + 1px)`,
    [theme.breakpoints.up('sm')]: {
        width: `calc(${theme.spacing(8)} + 1px)`,
    },
});

const DrawerHeader = styled('div')(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: "8%",
    padding: theme.spacing(0, 1),
    
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
}));


const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
    
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
       
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
        
    }),
}));

const appBarStyle =(open) =>  {
    return {
        //background: `${BAR_COLOR}!important`,
        justifyContent: open ? "end" : "space-between"
    }
    }

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
    ({ theme, open }) => ({
        
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
        boxSizing: 'border-box',
        ...(open && {
            ...openedMixin(theme),
            
            '& .MuiDrawer-paper': openedMixin(theme),
        }),
        ...(!open && {
            ...closedMixin(theme),
            '& .MuiDrawer-paper': closedMixin(theme),
        }),
    }),
);




export default function MiniDrawer({children, currentUser }) {
    const theme = useTheme();
    const [open, setOpen] = React.useState(false);
    //const dispatch = useDispatch()
    const handleDrawerOpen = () => {
        setOpen(!open);
    };

    const handleDrawerClose = () => {
        setOpen(!open);
    };

    const onClickLogout = () => {
        //logout(dispatch)
    }
    return(
        <Box sx={{ display: 'flex', margin:0 ,padding: 0,  }}>
                        <CssBaseline />

            <AppBar position="fixed" open={open}  >
                <Toolbar sx={appBarStyle(open)}>
        
                    <IconButton
                        color="primary"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        sx={{
                            color: "white",
                            marginRight: 5,
                            ...(open && { display: 'none' }),
                        }}
                    >
                        <MenuIcon />
                    </IconButton>
                    <IconButton
                    >
                        <AccountCircleIcon sx={{color:"#Fbfbfb"}} /> 
                        <Typography variant='body2' color="#Fbfbfb"> { currentUser}</Typography>
                        
                    </IconButton>
                    
                </Toolbar>

            </AppBar>

            <Drawer variant="permanent" open={open}>
                <DrawerHeader> 
                    
                    <IconButton onClick={handleDrawerOpen}>
                        {theme.direction === 'rtl' ? <ChevronRightIcon color='primary' /> : <ChevronLeftIcon color='primary' />}
                    </IconButton>
                    
                </DrawerHeader>
                
                <Divider />
                <Navbar isOpen={open} handleOpen={handleDrawerOpen} />
                <Divider />
                

            </Drawer>
             
            <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
                
                <DrawerHeader />
                {children}
            </Box>

        </Box>
    )
}