import React, { useState } from "react";
import Chatbot from "react-chatbot-kit";
import config from "../../config";
import ActionProvider from "../../actions/ActionProvider";
import MessageParserV1 from "../../actions/MessageParserV1";
import MessageParserV2 from "../../actions/MessageParserV2";
import MessageParserV3 from "../../actions/MessageParserV3";
import VersionSelector from "../EtiquetaSelector/EtiquetaSelector";
import { dummyVersion } from "../../sandbox/dummyData";
import { Grid } from "@mui/material";
const ChatbotComponent = () => {
  const [version, setVersion] = useState("V1");

  const handleChange = (event) => {
    setVersion(event.target.value);
  };

  return (
    <>
      <Grid container columnSpacing={5}>
        <Grid item xl={12} mb={2}>
          <VersionSelector handleChange={handleChange} currentValue={version} values={dummyVersion} />
        </Grid>
        <Grid item xl={12} alignContent="right">
          {version === "V1" ? (
            <Chatbot
              config={config}
              messageParser={MessageParserV1}
              headerText="Agente virtual EOL V1"
              placeholderText="Escribe aquí tu mensaje..."
              actionProvider={ActionProvider}
            />
          ) : version === "V2" ? (
            <Chatbot
              config={config}
              messageParser={MessageParserV2}
              headerText="Agente virtual EOL V2"
              placeholderText="Escribe aquí tu mensaje..."
              actionProvider={ActionProvider}
            />
          ) : (
            <Chatbot
              config={config}
              messageParser={MessageParserV3}
              headerText="Agente virtual EOL V3"
              placeholderText="Escribe aquí tu mensaje..."
              actionProvider={ActionProvider}
            />
          )}
        </Grid>
      </Grid>
    </>
  );
};

export default ChatbotComponent;
