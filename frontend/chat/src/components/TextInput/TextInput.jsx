import React from 'react'
import { TextField } from '@mui/material'

const TextInput = ({ label, name, handleChange, value, id, disabled, maxLength, required }) => {
  return (
    <TextField
                required={required}
                inputProps={{ maxLength: maxLength }}
                id={id}
                name={name}
                label={label}
                value={value || ""}
                size='lg'
                variant="outlined"
                fullWidth
                onChange={handleChange}
                disabled={disabled} />
  )
}

export default TextInput