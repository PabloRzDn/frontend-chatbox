import React from 'react'
import { Paper, Box, Container } from '@mui/material'
import ChatbotComponent from '../Chatbot/Chatbot'
const ChatComponent = () => {
  return (
    <div>
      <Container><Paper
    sx={{
        display:"flex",
        justifyContent:'center',
        bgcolor: 'background.paper',
        boxShadow: 1,
        borderRadius: 2,
        p: 2,
        width: 600,
        margin:10,
        padding:5
      }}
    >
        
            <ChatbotComponent/>
        
        </Paper>
        </Container></div>
  )
}

export default ChatComponent