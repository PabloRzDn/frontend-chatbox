import React from 'react'
import { Button } from '@mui/material'
import AddIcon from '@mui/icons-material/Add';

const IngresarButton = ({disabled, onClickIngresar}) => {
  return (
    <Button
    type='submit'
    disabled={disabled}
    size="large"
    onClick={onClickIngresar}
    color="info"
    title="Ingresar"
    variant="outlined"
    fullWidth={true}
>
    Ingresar
</Button>
  )
}

export default IngresarButton