import axios from 'axios';
import React from 'react';
import { BACKEND_URL } from '../redux/urls/backend.url';

const APICall = async (message) =>{

  const access = localStorage.getItem("access")
  const headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Authorization':`Bearer ${access}`
  }
  const params = {
    message: message,
  }
  try {
    
    const res = await axios.post(`${BACKEND_URL}/api/v2/messages/`, params, {headers: headers})
    return {data: res.data, status: res.status}
  } catch (error) {
    return {error: error.response, status: error.status}
  }
}

const MessageParser = ({ children, actions }) => {
  const parse = (message) =>  {
    
    
    //recibiendo message obtener la response desde el backend
    let data
    APICall(message).then(result=>{
    
      if (result.status===200){
        data = result.data['Post']
        //console.log(data)
        actions.handleHello(data);
      }
      else {
        data = 'Error'
        actions.handleHello(data);
      }
    })
   
  };

  return (
    <div>
      {React.Children.map(children, (child) => {
        return React.cloneElement(child, {
          parse: parse,
          actions,
        });
      })}
    </div>
  );
};

export default MessageParser;