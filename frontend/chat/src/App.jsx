
import React from "react";
import { BrowserRouter } from "react-router-dom";
import 'react-chatbot-kit/build/main.css';
import { Provider, useDispatch, useSelector} from "react-redux"
import store from "./redux/store/store";
import { AppRoutes } from "./routes/Index";

const   App = ()=> {
  return (
    <Provider store={store}>
     
      <BrowserRouter>
       <AppRoutes/>
       </BrowserRouter>
      
    </Provider>
    
  );
}

export default App;