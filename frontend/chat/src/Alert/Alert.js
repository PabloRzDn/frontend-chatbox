import Swal from "sweetalert2";

export const successAlert = (succesMessage) => {
    Swal.fire({
        icon: 'success',
        title: succesMessage,
        showConfirmButton: false,
        timer: 2000,
        heightAuto: true
    })
}

export const dangerAlert = (warningMessage) => {
    Swal.fire({
        icon: 'warning',
        title: warningMessage,
        showConfirmButton: false,
        timer: 2000,
        heightAuto: true
    })
}