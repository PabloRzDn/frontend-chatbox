import { BACKEND_URL } from "./backend.url"

export const frasesUrl= (page, pageSize) =>{
    return `${BACKEND_URL}/api/mantainer/dataphrases/?page=${page}&page_size=${pageSize}`
}