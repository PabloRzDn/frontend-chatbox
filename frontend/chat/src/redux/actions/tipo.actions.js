import { GET_TIPO_SUCCESS, GET_TIPO_FAIL } from "../types/tipo.types"
import { TiposUrl } from "../urls/tipo.url"
import axios from "axios"

export const getTipos = () => async dispatch => {
    const access = localStorage.getItem("access")
    const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization':`Bearer ${access}`
      }
    try {
        const res = await axios.get(TiposUrl, {headers: headers})
        dispatch({
            type: GET_TIPO_SUCCESS,
            payload: res.data
        })
    } catch (error) {
        dispatch({
            type: GET_TIPO_FAIL,
            
        })
    }
}