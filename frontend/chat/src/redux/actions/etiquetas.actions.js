import axios from "axios";
import { GET_ETIQUETAS_SUCCESS, GET_ETIQUETAS_FAIL } from "../types/etiqueta.types";
import { etiquetasUrl } from "../urls/etiquetas.url";


export const getEtiquetas = () => async dispatch => {
    const access = localStorage.getItem("access")
    const headers = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization':`Bearer ${access}`
      }
    try {
        const res = await axios.get(etiquetasUrl, {headers: headers})
        dispatch({
            type: GET_ETIQUETAS_SUCCESS,
            payload: res.data
        })
    } catch (error) {
        dispatch({
            type: GET_ETIQUETAS_FAIL,
            
        })
    }
}