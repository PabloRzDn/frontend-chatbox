import axios from "axios";
import { postFraseUrl } from "../urls/frase.url";

export const postFrase = async (props) => {
  const dataToPost = {
    cuerpo: [props.cuerpo],
    ticket_id: 4,
    tipo_mensaje: props.tipo_mensaje,
    etiqueta: props.etiqueta,
  };

  const access = localStorage.getItem("access");
  const headers = {
    "Content-Type": "application/json",
    Accept: "application/json",
    Authorization: `Bearer ${access}`,
  };
  try {
    const res = await axios.post(postFraseUrl,dataToPost ,{ headers: headers });
    return {
        data:res.data,
        status:res.status
    } 
  } catch (error) {
    console.log(error);
    return {
        data: error.response.data,
        status:error.response.status
    }
  }
};
