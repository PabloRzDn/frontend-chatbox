import axios from "axios";
import { postLoginUrl } from "../urls/user.url";
import { LOGIN_USER_SUCCESS, LOGIN_USER_FAIL } from "../types/user.types";

export const login =  ( userData )  =>  async dispatch => {
    try {
        const res = await axios.post(postLoginUrl, userData)
        dispatch({type: LOGIN_USER_SUCCESS, payload: res.data})
        return {
            data:res.data,
            status:res.status
        } 
    } catch (error) {
        dispatch({type:LOGIN_USER_FAIL})
        return {
            data: error.response.data,
            status:error.response.status
        }
    }
    
}
