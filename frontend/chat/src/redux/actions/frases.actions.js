import axios from "axios";
import { GET_FRASES_SUCESS, GET_FRASES_FAIL } from "../types/frases.types";
import { BACKEND_URL } from "../urls/backend.url";
import { frasesUrl } from "../urls/frases.url";

export const getFrases= (props) => async dispatch =>{
    const access = localStorage.getItem("access")
    const headers = {
        "Content-Type":"application/json",
        "Accept":"application/json",
        "Authorization":`Bearer ${access}`
    }
    try {
        const res = await axios.get(frasesUrl(props.page, props.pageSize), {headers:headers})
        dispatch({
            type: GET_FRASES_SUCESS,
            payload: res.data
        })
    } catch (error) {
        console.log(error)
        dispatch({
            type: GET_FRASES_FAIL,
            
        })
    }
}