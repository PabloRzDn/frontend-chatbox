import { combineReducers } from "redux";
import etiquetasReducer from "./etiquetas.reducer";
import frasesReducer from "./frases.reducer";
import tiposReducer from "./tipos.reducer";
import loginReducer from "./user.reducer";

export default combineReducers({ login: loginReducer, frases: frasesReducer, tipos: tiposReducer, etiquetas: etiquetasReducer });
