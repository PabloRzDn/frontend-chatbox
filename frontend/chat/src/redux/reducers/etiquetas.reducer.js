import { GET_ETIQUETAS_SUCCESS, GET_ETIQUETAS_FAIL } from "../types/etiqueta.types"
const initialState= {
    count:null,
    next: null,
    previous: null,
    results:[]
}


export default function etiquetasReducer( state= initialState, action){
    const { type, payload} = action
    switch (type) {
        case GET_ETIQUETAS_SUCCESS:
            
            return{
                count: payload.count,
                next: payload.next,
                previous: payload.previous,
                results: payload.results
            }
        
        case GET_ETIQUETAS_FAIL:
            return state
        default:
            return state
    }
}