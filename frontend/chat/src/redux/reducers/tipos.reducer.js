import { GET_TIPO_SUCCESS, GET_TIPO_FAIL } from "../types/tipo.types"

const initialState= {
    count:null,
    next: null,
    previous: null,
    results:[]
}


export default function tiposReducer( state= initialState, action){
    const { type, payload} = action
    switch (type) {
        case GET_TIPO_SUCCESS:
            
            return{
                count: payload.count,
                next: payload.next,
                previous: payload.previous,
                results: payload.results
            }
        
        case GET_TIPO_FAIL:
            return state
        default:
            return state
    }
}