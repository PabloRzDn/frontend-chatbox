import { LOGIN_USER_SUCCESS, LOGIN_USER_FAIL } from "../types/user.types";

const initialState = {
    access:"",
    refresh:""
}

export default function loginReducer( state= initialState, action){
    const {type, payload} = action
    switch (type) {
        case LOGIN_USER_SUCCESS:
            return {
                access:payload.access,
                refresh: payload.refresh
            }
        case LOGIN_USER_FAIL:
            return state
    
        default:
            return state
    }
}