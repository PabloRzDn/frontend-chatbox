import { GET_FRASES_SUCESS, GET_FRASES_FAIL } from "../types/frases.types";

const initialState= {
    count:null,
    next: null,
    previous: null,
    results:[]
}


export default function frasesReducer( state= initialState, action){
    const { type, payload} = action
    switch (type) {
        case GET_FRASES_SUCESS:
            
            return{
                count: payload.count,
                next: payload.next,
                previous: payload.previous,
                results: payload.results
            }
        
        case GET_FRASES_FAIL:
            return state
        default:
            return state
    }
}